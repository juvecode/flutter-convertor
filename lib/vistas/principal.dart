import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc_practice2/componentes/componentes.dart';

class VistaPrincipal extends StatelessWidget {
  const VistaPrincipal({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final estado = context.watch<BlocContador>().state;
    return estado.map((iniciandome) => const VistaIniciandome(), 
                numero: (numero) => VistaNumero(numero.numero, numero.tipoNumeral), 
                imagen: (nombre) => VistaImagen(nombre.nombre),
                problema: (mensaje) => VistaError(mensaje.mensaje));

    // if(estado is Iniciandome) {
    //   return const VistaIniciandome();
    // }
    // if(estado is Numero) {
    //   return VistaNumero(estado.numero);
    // }
    // if(estado is Imagen) {
    //   return VistaImagen(estado.nombre);
    // }
    // return Container(
      
    // );
  }
}






