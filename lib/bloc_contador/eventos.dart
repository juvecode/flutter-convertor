import 'package:bloc_practice2/dominios/interfase_contador.dart';

class Evento{}

class Inicializado extends Evento {}

class Modificado extends Evento {
  final int enCuanto;

  Modificado(this.enCuanto);
}

class ModificadoNumerales extends Evento {
  final int cualNumeral;

  ModificadoNumerales(this.cualNumeral);
}

class RepositorioCambiado extends Evento {
  final InterfaseRepositorios repositorio;

  RepositorioCambiado(this.repositorio);

}