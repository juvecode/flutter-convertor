import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_practice2/app/constantes.dart';
import 'package:bloc_practice2/bloc_contador/estados.dart';
import 'package:bloc_practice2/bloc_contador/eventos.dart';
import 'package:bloc_practice2/dominios/interfase_contador.dart';

class BlocContador extends Bloc<Evento, Estado2> {
  BlocContador(this.repositorio) : super(const Iniciandome()){
    // METHODS
    void _onInicializado(Inicializado event, Emitter<Estado2> emit) async {
      final valorRecibido = await repositorio.obtenerNumero(minimo: 1, maximo: 10);
      valorRecibido.fold(
        (cadena) => {
          emit(Problema(cadena))
        },
        (entero) {
          numero = entero;
          emit(dependeNumero(numero));
        });
    }

    void _onModificado(Modificado event, Emitter<Estado2> emit){
      numero = numero + event.enCuanto;
      emit(dependeNumero(numero));
    }

    FutureOr<void> _onModificadoNumerales(ModificadoNumerales event, Emitter<Estado2> emit) {
        numeralSeleccionado = event.cualNumeral;
        emit(dependeNumero(numero));
    }

    FutureOr<void> _onRepositorioCambiado(RepositorioCambiado event, Emitter<Estado2> emit) {
      repositorio = event.repositorio;
      add(Inicializado());
    } 


    // INIT
    // Future.delayed( const Duration(seconds: 2), (){
    //   add(Inicializado());    
    // });

    // EVENTS
    on<Inicializado>(_onInicializado);
    on<Modificado>(_onModificado);
    on<ModificadoNumerales>(_onModificadoNumerales);
    on<RepositorioCambiado>(_onRepositorioCambiado);
    add(Inicializado());
  }

  int numero = 0;
  InterfaseRepositorios repositorio;
  int numeralSeleccionado = 0;

  Estado2 dependeNumero(int numero) {
    if (numero == numeroPele) {
        return (const Imagen(direccionImagenPele));
      } 
    return (Numero(numero, numeralSeleccionado));
  }
}

