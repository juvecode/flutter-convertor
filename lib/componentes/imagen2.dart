import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';
import 'package:bloc_practice2/bloc_contador/estados.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class VistaImagen extends StatelessWidget {
  const VistaImagen(this.nombre, { Key? key }) : super(key: key);
  final String nombre;

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;
    final String nombre = (estado as Imagen).nombre;
    return Center(
      child: Image.asset(nombre)
    );
  }
}