import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';
import 'package:bloc_practice2/bloc_contador/eventos.dart';
import 'package:bloc_practice2/repositorios/local.dart';
import 'package:bloc_practice2/repositorios/remoto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VistaError extends StatelessWidget {
  const VistaError(this.mensajeError, { Key? key }) : super(key: key);
  final String mensajeError;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(mensajeError, style: const TextStyle(fontSize: 25.0),),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: ElevatedButton.icon(
              onPressed: () {
                context
                  .read<BlocContador>()
                  .add(RepositorioCambiado(context.read<RepositorioRemoto>()));
              },
              label: const Text('Intentar con repositorio remoto'),
              icon: const Icon(Icons.local_offer_outlined),
            ),
            ),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: ElevatedButton.icon(
                onPressed: () {
                  context
                    .read<BlocContador>()
                    .add(RepositorioCambiado(context.read<RepositorioLocal>()));
                },
                label: const Text('Intentar con repositorio local', style: TextStyle(fontSize: 15.0),),
                icon: const Icon(Icons.local_offer ),
              ),
            ),
          ],),
      ));
  }
}

