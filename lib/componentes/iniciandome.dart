import 'package:bloc_practice2/app/constantes.dart';
import 'package:flutter/material.dart';

class VistaIniciandome extends StatelessWidget {
  const VistaIniciandome({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircularProgressIndicator(),
          const Text(mensajeDeInicializacion)
        ]
      ),
    );
  }
}